var express = require('express');
var _ = require('lodash');
var app = express();
const { Client } = require('pg');

// Conexão com o banco de dados PostgreSQL

const connectionString = 'postgres://postgres:colocarsenhaaqui@localhost:5432/nomedabasededados';

const client = new Client({
    connectionString: connectionString
});

client.connect();

//-----------------------------

// Criar de tabela

//client.query('CREATE TABLE teste2 ( id SERIAL PRIMARY KEY, aerogerador varchar(6) NOT NULL, velocidade numeric(5,1) NOT NULL, potencia numeric(5,1) NOT NULL, timestamp TIMESTAMPTZ NOT NULL DEFAULT NOW());');

//-----------------------------
// Gerar de dados

function geradorDados(){
  var velocidade = 0;
  var potencia = 0;

  for (var i = 1; i < 32; i++){
    velocidade = Math.random() * (18 - 0);
    potencia = Math.random() * (2200 - 500) + 500;
    client.query('INSERT INTO teste2 (aerogerador, velocidade, potencia) values ($1, $2, $3)',['AERO'+i.toString(), velocidade, potencia], function (err, result) {
      if (err) {
        console.log(err);
      }
    });
  }
};

geradorDados();
setInterval( geradorDados, 10*1000);
//-----------------------------


